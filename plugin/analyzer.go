// This must be package main
package main

import (
	linters "gitlab.com/gitlab-org/language-tools/go/linters/goargs"
	"golang.org/x/tools/go/analysis"
)

type analyzerPlugin struct{}

// This must be implemented
func (*analyzerPlugin) GetAnalyzers() []*analysis.Analyzer {
	return []*analysis.Analyzer{
		linters.GoArgsAnalyzer,
	}
}

// This must be defined and named 'AnalyzerPlugin'
//
//nolint:unused,deadcode,golint
var AnalyzerPlugin analyzerPlugin
