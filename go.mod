module gitlab.com/gitlab-org/language-tools/go/linters/goargs

go 1.22

toolchain go1.23.3

require golang.org/x/tools v0.6.0
