ARG GOLANG_VERSION
ARG SHELLCHECK_VERSION

FROM koalaman/shellcheck:v${SHELLCHECK_VERSION} as shellcheck
FROM golang:${GOLANG_VERSION} as build

WORKDIR /build

ARG GOLANGLINT_VERSION

RUN git config --global advice.detachedHead false && \
    git clone https://github.com/golangci/golangci-lint.git --no-tags --depth 1 -b "v${GOLANGLINT_VERSION}" repo && \
    cd repo && \
    CGO_ENABLED=1 go build -o /build/golangci-lint -ldflags "-s -w \
        -X main.version=${GOLANGLINT_VERSION} -X main.commit=$(git rev-parse --short HEAD) -X main.date=$(date -u '+%FT%TZ')" \
        ./cmd/golangci-lint/

COPY . /build/goargs
RUN cd goargs && \
    CGO_ENABLED=1 go build -buildmode=plugin -o /build/goargs.so plugin/analyzer.go

FROM golang:${GOLANG_VERSION}

# Install required dependencies
RUN apt-get update -yq && \
    apt-get install -yq --no-install-recommends make jq && \
    rm -rf /var/lib/apt/lists/*

# don't place it into $GOPATH/bin because Drone mounts $GOPATH as volume
COPY --from=build /build/golangci-lint /usr/bin/
COPY --from=build /build/goargs.so /usr/lib/
COPY --from=shellcheck /bin/shellcheck /bin/
