package linters

import (
	"fmt"
	"go/ast"
	"strings"

	"golang.org/x/tools/go/analysis"
)

//nolint:gocognit
func getExprAsString(pass *analysis.Pass, expr ast.Expr) string {
	switch t := expr.(type) {
	case *ast.Ident:
		return t.Name
	case *ast.Ellipsis:
		if t.Elt == nil {
			return "..."
		}
		return fmt.Sprintf("%s...", getExprAsString(pass, t.Elt))
	case *ast.ParenExpr:
		return fmt.Sprintf("(%s)", getExprAsString(pass, t.X))
	case *ast.SelectorExpr:
		return fmt.Sprintf("%s.%s", getExprAsString(pass, t.X), t.Sel.Name)
	case *ast.StarExpr:
		return "*" + getExprAsString(pass, t.X)
	case *ast.FuncType:
		return getFuncTypeAsString(pass, t, false)
	case *ast.ChanType:
		arrow := "chan"
		if t.Arrow.IsValid() {
			switch t.Dir {
			case ast.RECV:
				arrow = "<-chan"
			case ast.SEND:
				arrow = "chan<-"
			}
		}
		return fmt.Sprintf("%s %s", arrow, getExprAsString(pass, t.Value))
	case *ast.MapType:
		return fmt.Sprintf("map[%s]%s", getExprAsString(pass, t.Key), getExprAsString(pass, t.Value))
	case *ast.ArrayType:
		return fmt.Sprintf("[%s]%s", getExprAsString(pass, t.Len), getExprAsString(pass, t.Elt))
	case *ast.InterfaceType:
		return "{}interface"
	case *ast.StructType:
		return "{}struct"
	case nil:
		return ""
	default:
		pass.ReportRangef(expr, "expression type not supported")
		return ""
	}
}

func getFuncTypeAsString(pass *analysis.Pass, funcType *ast.FuncType, named bool) string {
	argList := ""
	if funcType.Func.IsValid() && !named {
		argList += "func"
	}

	// calculate parameters
	argList += getFuncParamsAsString(pass, funcType)

	// calculate function results
	funcResults := funcType.Results
	if funcResults != nil {
		argList += " " + getFuncResultsAsString(pass, funcResults)
		if funcResults.Closing.IsValid() {
			argList += " {"
		}
	}

	return argList
}

func getFuncParamsAsString(pass *analysis.Pass, funcType *ast.FuncType) string {
	var tokens []string
	for _, param := range funcType.Params.List {
		nameTokens := getParameterNames(param)
		tokens = append(tokens, strings.Join(nameTokens, ", ")+" "+getExprAsString(pass, param.Type))
	}
	return fmt.Sprintf("(%s)", strings.Join(tokens, ", "))
}

func getFuncResultsAsString(pass *analysis.Pass, funcResults *ast.FieldList) string {
	var resultListTokens []string
	for _, result := range funcResults.List {
		if result.Names != nil {
			for _, paramName := range result.Names {
				resultListTokens = append(resultListTokens, paramName.String())
			}
		}
		resultListTokens = append(resultListTokens, getExprAsString(pass, result.Type))
	}

	resultsStr := strings.Join(resultListTokens, ", ")
	if len(funcResults.List) == 0 {
		return resultsStr
	}
	return "(" + resultsStr + ")"
}

func getParameterNames(param *ast.Field) []string {
	var nameTokens []string
	for _, paramName := range param.Names {
		nameTokens = append(nameTokens, paramName.String())
	}
	return nameTokens
}
